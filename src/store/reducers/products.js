import { ADD_PRODUCT, DEL_PRODUCT, TOGGLE_CART } from "../actionTypes";

const initialState = {
  products: [
    {
      id: "id1",
      img: [
        "bitmap.jpg",
        "bitmap-1.jpg",
        "bitmap-2.jpg",
        "bitmap-3.jpg",
        "bitmap-4.jpg",
        "bitmap-5.jpg",
      ],
      type: "рубашки",
      name: "рубашка на пуговицах",
      price: "320",
      balance: 20,
    },
    {
      id: "id2",
      img: ["bitmap-6.jpg"],
      type: "рубашки",
      name: "Рубашка с принтом",
      price: "170",
      balance: 11,
    },
    {
      id: "id3",
      img: ["bitmap-7.jpg"],
      type: "кроссовки",
      name: "Кроссовки «Kaiwa» Y3",
      price: "240",
      balance: 134,
    },
    {
      id: "id4",
      img: ["bitmap-8.jpg"],
      type: "кроссовки",
      name: "Кроссовки с пряжками",
      price: "1240",
      balance: 11,
    },
    {
      id: "id5",
      img: ["bitmap-9.jpg"],
      type: "брюки",
      name: "Укороченные зауженные брюки",
      price: "390",
      balance: 11,
    },
    {
      id: "id6",
      img: ["bitmap-10.jpg"],
      type: "кроссовки",
      name: "Кроссовки с шнурками",
      price: "647",
      balance: 7,
    },
  ],
  cart: [
    {
      id: "id1",
      quantity: 1,
    },
    {
      id: "id3",
      quantity: 2,
    },
  ],
  isCartOpen: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return {
        ...state,
        cart: state.cart.some(({ id }) => id === action.payload)
          ? state.cart.map((item) =>
              item.id === action.payload
                ? {
                    ...item,
                    quantity: item.quantity + 1,
                  }
                : item
            )
          : [...state.cart, { id: action.payload, quantity: 1 }],
      };
    case DEL_PRODUCT:
      return {
        ...state,
        cart: state.cart.some(
          ({ id, quantity }) => id === action.payload && quantity > 1
        )
          ? state.cart.map((item) =>
              item.id === action.payload
                ? {
                    ...item,
                    quantity: item.quantity - 1,
                  }
                : item
            )
          : state.cart.filter(({ id }) => id !== action.payload),
      };
    case TOGGLE_CART:
      return { ...state, isCartOpen: !state.isCartOpen };
    default:
      return state;
  }
};
