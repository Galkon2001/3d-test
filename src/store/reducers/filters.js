import { CHANGE_TYPE, CHANGE_SORT } from "../actionTypes";

const initialState = {
  type: "все",
  sort: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_TYPE:
      return { ...state, type: action.payload };
    case CHANGE_SORT:
      return { ...state, sort: action.payload };
    default:
      return state;
  }
};
