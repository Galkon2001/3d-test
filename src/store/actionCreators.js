import {
  ADD_PRODUCT,
  DEL_PRODUCT,
  CHANGE_TYPE,
  CHANGE_SORT,
  TOGGLE_CART,
} from "./actionTypes";

const addProduct = (payload) => ({
  type: ADD_PRODUCT,
  payload,
});
const delProduct = (payload) => ({
  type: DEL_PRODUCT,
  payload,
});
const changeType = (payload) => {
  return {
    type: CHANGE_TYPE,
    payload,
  };
};
const changeSort = (payload) => ({
  type: CHANGE_SORT,
  payload,
});
const toggleCart = () => ({
  type: TOGGLE_CART,
});

export { addProduct, delProduct, changeType, changeSort, toggleCart };
