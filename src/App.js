import React from "react";
import { Switch, Route } from "react-router-dom";
import "normalize.css";
import "./style.scss";
import { Main, ProductPage, ShoppingCart } from "./containers";
import { connect } from "react-redux";

function App({ isCartOpen }) {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Main} />
        <Route path="/:id" component={ProductPage} />
      </Switch>
      {isCartOpen && <ShoppingCart />}
    </div>
  );
}
const mapState = ({ products }) => ({ isCartOpen: products.isCartOpen });

export default connect(mapState)(App);
