import React, { useMemo } from "react";
import { connect } from "react-redux";
import { toggleCart, addProduct, delProduct } from "../../store/actionCreators";
import { ReactComponent as Close } from "../../svg/close.svg";

const ShoppingCart = ({
  products,
  cart,
  toggleCart,
  addProduct,
  delProduct,
}) => {
  const cartItems = useMemo(() => {
    const items = [];
    products.forEach((product) => {
      cart.forEach((cartItem) => {
        if (cartItem.id === product.id) {
          items.push({ ...product, quantity: cartItem.quantity });
        }
      });
    });
    return items;
  }, [products, cart]);
  return (
    <div className="shoppingCart_container">
      {cartItems.map((item) => (
        <div key={item.id} className="shoppingCart_item">
          <div
            style={{ backgroundImage: `url(img/products/${item.img[0]})` }}
            className="shoppingCartItem_img"
          ></div>
          <div className="shoppingCartItem_info">
            <p className="shoppingCartItem_name">{item.name}</p>
            <div className="shoppingCartItem_sumary">
              <span className="shoppingCartItem_quantity">
                <span
                  onClick={() => {
                    delProduct(item.id);
                  }}
                  className="shoppingCartItem_decrement"
                >
                  -
                </span>
                <span className="shoppingCartItem_value">{item.quantity}</span>
                <span
                  onClick={() => {
                    addProduct(item.id);
                  }}
                  className="shoppingCartItem_increment"
                >
                  +
                </span>
              </span>
              <span className="shoppingCartItem_price">${item.price}</span>
            </div>
          </div>
        </div>
      ))}
      <div className="shoppingCartSum">
        <span>Итого</span>
        <span>
          $
          {cartItems.reduce((sum, cur) => {
            return sum + cur.price * cur.quantity;
          }, 0)}
        </span>
      </div>
      <button
        onClick={() => alert("under construction")}
        className="shoppingCart_Buy"
      >
        купить
      </button>
      <Close onClick={toggleCart} className="shoppingCart_close" />
    </div>
  );
};

const mapState = ({ products }) => products;
const mapDispatch = (dispatch) => ({
  toggleCart: () => dispatch(toggleCart()),
  addProduct: (id) => dispatch(addProduct(id)),
  delProduct: (id) => dispatch(delProduct(id)),
});

export default connect(mapState, mapDispatch)(ShoppingCart);
