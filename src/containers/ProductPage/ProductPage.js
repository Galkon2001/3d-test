import React from "react";
import { connect } from "react-redux";
import { ProductCard } from "../../components";
import { Header } from "../../containers";
import { addProduct } from "../../store/actionCreators";

const ProductPage = ({ products, history, match, addProduct }) => {
  const product = products.find((product) => product.id === match.params.id);
  return (
    <div>
      <Header />
      <span>
        Заглушка демонстрирующая работоспособность Роутинга и функциональность
        добавления в корзину
      </span>
      <div
        onClick={() => {
          history.push("");
        }}
        className="back"
        style={{
          fontStyle: "bold",
          color: "red",
          background: "yellow",
          cursor: "pointer",
        }}
      >
        back
      </div>
      {product ? (
        <>
          <ProductCard
            onClick={() => {
              history.push("");
            }}
            card={product}
          />
          <button
            onClick={() => addProduct(product.id)}
            className="shoppingCart_Buy dark"
          >
            Добавить в корзину
          </button>
        </>
      ) : (
        "This products does not exist. Sorry"
      )}
    </div>
  );
};

const mapState = ({ products }) => ({ products: products.products });
const mapDispatch = (dispatch) => ({
  addProduct: (id) => {
    dispatch(addProduct(id));
  },
});

export default connect(mapState, mapDispatch)(ProductPage);
