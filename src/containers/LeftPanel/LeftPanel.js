import React, { useState } from "react";
import { connect } from "react-redux";
import { changeType, changeSort } from "../../store/actionCreators";
import { ReactComponent as Arrow } from "../../svg/arrow.svg";

const types = ["все", "Плащи", "Кроссовки", "Рубашки", "Брюки"];
const sortOptions = [
  "От дорогих к дешевым",
  "От дешевых к дорогим",
  "Популярные",
  "Новые",
];

const LeftPanel = ({ type, sort, changeType, changeSort }) => {
  const [popOpen, setPopOpen] = useState(false);
  return (
    <div className="leftPanel_container">
      <ul className="leftPanel_types">
        {types.map((option, i) => (
          <li
            onClick={() => {
              changeType(option);
            }}
            key={i}
            className={`leftPanel_typesOption${
              type && type.toLowerCase() === option.toLowerCase()
                ? " active"
                : ""
            }`}
          >
            {option}
          </li>
        ))}
      </ul>
      <p className="leftPanel_sort" onClick={() => setPopOpen(!popOpen)}>
        Сортировать <Arrow className={popOpen ? "open" : ""} />
      </p>
      {popOpen && (
        <ul className="leftPanel_sortPopUp">
          {sortOptions.map((option, i) => (
            <li
              onClick={() => {
                setPopOpen(false);
                changeSort(option);
              }}
              key={i}
              className={`leftPanel_sortOption${
                sort === option ? " active" : ""
              }`}
            >
              {option}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

const mapState = ({ filters }) => ({
  type: filters.type,
  sort: filters.sort,
});
const mapDispatch = (dispatch) => {
  return {
    changeType: (payload) => dispatch(changeType(payload)),
    changeSort: (payload) => dispatch(changeSort(payload)),
  };
};

export default connect(mapState, mapDispatch)(LeftPanel);
