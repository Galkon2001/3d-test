export { default as Main } from "./Main/Main";
export { default as Header } from "./Header/Header";
export { default as MainPanel } from "./MainPanel/MainPanel";
export { default as LeftPanel } from "./LeftPanel/LeftPanel";
export { default as ShoppingCart } from "./ShoppingCart/ShoppingCart";
export { default as ProductPage } from "./ProductPage/ProductPage";
