import React from "react";
import { Header, MainPanel, LeftPanel } from "../";

const Main = ({ history }) => (
  <article className="main">
    <Header />
    <div className="main_container">
      <LeftPanel />
      <MainPanel history={history} />
    </div>
  </article>
);

export default Main;
