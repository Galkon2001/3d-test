import React from "react";
import { connect } from "react-redux";
import { toggleCart } from "../../store/actionCreators";
import { ReactComponent as Cart } from "../../svg/shopingCart.svg";

const Header = ({ quantity, isCartOpen, toggleCart }) => {
  return (
    <header className="header">
      <img className="header_logo" src="img/logo.jpg" alt="logo" />
      <div onClick={toggleCart} className="cart_container">
        <Cart className="cart_icon" />
        <span className="cart_quantity">{quantity}</span>
      </div>
    </header>
  );
};

const mapState = ({ products }) => ({
  quantity: products.cart.length,
  isCartOpen: products.isCartOpen,
});
const mapDispatch = (dispatch) => ({
  toggleCart: () => dispatch(toggleCart()),
});

export default connect(mapState, mapDispatch)(Header);
