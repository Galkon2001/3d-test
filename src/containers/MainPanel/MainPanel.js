import React, { useMemo } from "react";
import { ProductCard } from "../../components";
import { connect } from "react-redux";

const MainPanel = ({ products = [], cart, activeType, sort, history }) => {
  const sortedProducts = useMemo(() => {
    const filteredProducts = products.filter(
      ({ type }) =>
        activeType === "все" || activeType.toLowerCase() === type.toLowerCase()
    );
    switch (sort) {
      case "От дорогих к дешевым":
        return filteredProducts.sort(
          ({ price: price1 }, { price: price2 }) => price2 - price1
        );
      case "От дешевых к дорогим":
        return filteredProducts.sort(
          ({ price: price1 }, { price: price2 }) => price1 - price2
        );
      default:
        return filteredProducts;
    }
  }, [activeType, products, sort]);
  return (
    <div className="mainPanel_container">
      {sortedProducts.map((card) => (
        <ProductCard
          onClick={() => {
            history.push(card.id);
          }}
          key={card.id}
          card={card}
        />
      ))}
    </div>
  );
};

const mapState = ({ products, filters }) => ({
  products: products.products,
  cart: products.cart,
  activeType: filters.type,
  sort: filters.sort,
});
export default connect(mapState)(MainPanel);
