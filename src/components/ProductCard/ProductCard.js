import React, { useState } from "react";

export default ({ card, onClick }) => {
  const [currentImg] = useState(card.img[0]);
  return (
    <div className="productCard_container" onClick={onClick}>
      <div
        className="productCard_img"
        style={{ backgroundImage: `url(img/products/${currentImg})` }}
      ></div>
      <div className="productCard_title">
        <p className="productCard_type">{card.type}</p>
        <p className="productCard_name">{card.name}</p>
        <p className="productCard_price">$ {card.price}</p>
        <p className="productCard_balance">на складе: {card.balance}</p>
      </div>
    </div>
  );
};
